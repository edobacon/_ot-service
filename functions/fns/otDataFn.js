const cors = require('cors')({ origin: true });
const { firestore, validateToken } = require('../admin');
const moment = require('moment-timezone');
moment.tz.setDefault('America/Santiago');
moment.locale('es');

const otDataFn = async (req, res) => {
  cors(req, res, async () => {
    try {
      if (req.method !== 'POST') res.status(404).json({ message: 'OPTS' });
      const { uid, section, approval, diagnosis, workshop, delivery, payment, status, token } = req.body;
      if (!uid || !section || !status || !token) return res.status(401).json({ message: 'MISSING DATA' });
      const validatedUser = await validateToken(token);
      console.log({ validatedUser });
      if (!validatedUser.ok) return res.status(401).json({ message: 'UNAUTHORIZED' });
      const userForbiddenSection = ['APPROVAL', 'DELIVERY', 'PAYMENT'];
      const notForbiddenRoles = ['ADMIN_ROLE', 'SUPERVISOR_ROLE']; 
      if (userForbiddenSection.includes(section) && !notForbiddenRoles.includes(validatedUser.role)) res.status(403).json({ message: 'FORBIDDEN' });

      if (section === 'APPROVAL' && !approval) return res.status(401).json({ message: 'MISSING DATA' });
      if (section === 'DIAGNOSIS' && !diagnosis) return res.status(401).json({ message: 'MISSING DATA' });
      if (section === 'WORKSHOP' && !workshop) return res.status(401).json({ message: 'MISSING DATA' });
      if (section === 'DELIVERY' && !delivery) return res.status(401).json({ message: 'MISSING DATA' });
      if (section === 'PAYMENT' && !payment) return res.status(401).json({ message: 'MISSING DATA' });
      let data = null;
      section === 'APPROVAL' && (data = {approval});
      section === 'DIAGNOSIS' && (data = {diagnosis});
      section === 'WORKSHOP' && (data = {workshop});
      section === 'DELIVERY' && (data = {delivery});
      section === 'PAYMENT' && (data = {payment});
      if (data) {
        data.status = status;
        data.lastUpdateBy = validatedUser.uid;
        data.updatedAt = +moment().format('x');
        const updated = await firestore.doc(`ots/${uid}`).set(data, { merge: true });
        if (updated) return res.status(200).json({ message: 'UPDATED' });
      }
      return res.status(500).json({ message: 'ERROR' });
    } catch (error) {
      console.log(error.message);
      return res.status(500).json({ message: error.toString() });
    }
  });
};

module.exports = otDataFn;