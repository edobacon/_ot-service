const cors = require('cors')({ origin: true });
const { firestore, validateToken } = require('../admin');
const moment = require('moment-timezone');
moment.tz.setDefault('America/Santiago');
moment.locale('es');

const otInitialDataFn = async (req, res) => {
  cors(req, res, async () => {
    try {
      if (req.method !== 'POST') res.status(404).json({ message: 'OPTS' });
      const { token, uid, dni, id, status, client, state, services } = req.body;
      if (!token || !id || !status || !client || !services || !dni)
        return res.status(401).json({ message: 'MISSING DATA' });
      const validatedUser = await validateToken(token);
      if (!validatedUser.ok)
        return res.status(401).json({ message: 'UNAUTHORIZED' });
      if (uid) {
        let diagnosisAt = 0;
        let deliveryAt = 0;
        if (services.diagnosisDate.timestamp)
          diagnosisAt = services.diagnosisDate.timestamp;
        if (services.deliveryDate.timestamp)
          deliveryAt = services.deliveryDate.timestamp;
        await firestore.doc(`ots/${uid}`).set(
          {
            id,
            status,
            clientUID: client,
            clientDni: dni,
            state,
            services,
            updatedAt: +moment.utc().format('x'),
            lastUpdateBy: validatedUser.uid,
            diagnosisAt,
            deliveryAt
          },
          { merge: true }
        );
        return res.status(200).json({ message: 'UPDATED' });
      } else {
        let diagnosisAt = 0;
        let deliveryAt = 0;
        if (services.diagnosisDate.timestamp)
          diagnosisAt = services.diagnosisDate.timestamp;
        if (services.deliveryDate.timestamp)
          deliveryAt = services.deliveryDate.timestamp;
        const created = await firestore.collection('ots').add({
          id,
          status,
          clientUID: client,
          clientDni: dni,
          state,
          services,
          createdAt: +moment.utc().format('x'),
          createdBy: validatedUser.uid,
          updatedAt: 0,
          diagnosisAt,
          deliveryAt
        });
        const docId = created.id;
        return res.status(200).json({ message: 'CREATED', uid: docId });
      }
    } catch (error) {
      console.log(error.message);
      return res.status(500).json({ message: error.toString() });
    }
  });
};

module.exports = otInitialDataFn;
