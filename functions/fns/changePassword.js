const cors = require('cors')({ origin: true });
const { auth, validateToken } = require('../admin');
const moment = require('moment-timezone');
moment.tz.setDefault('America/Santiago');
moment.locale('es');

const changePassword = async (req, res) => {
  cors(req, res, async () => {
    if (req.method !== 'POST') res.status(404).json({ message: 'OPTS' });
    try {
      const { uid, password, token } = req.body;
      const validatedUser = await validateToken(token);
      console.log({ validatedUser });
      if (!validatedUser.ok || validatedUser.uid !== uid)
        return res.status(403).json({ message: 'FORBIDDEN' });

      const result = await auth.updateUser(uid, { password });
      if (result.uid)
        return res.status(200).json({ message: 'PASSWORD CHANGED' });
      return res.status(500).json({ message: 'ERROR' });
    } catch (error) {
      return res.status(500).json({ message: error.toString() });
    }
  });
};

module.exports = changePassword;
