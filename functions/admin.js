/* eslint-disable max-len */
const admin = require('firebase-admin');
const serviceAccount = require('./otservice-7db9b-firebase-adminsdk-laylh-ed52b4896e.json');
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://lms-dev-bb115.firebaseio.com'
});

const firestore = admin.firestore();

const auth = admin.auth();

async function validateToken(token, userData = false) {
  try {
    const decoded = await admin.auth().verifyIdToken(token);
    console.log({ decoded });
    const { uid, role } = decoded;
    if (!userData && uid) return { ok: true, uid, role };
    else if (!userData && !uid) return { ok: false };
    const user = await admin.firestore().doc(`users/${uid}`).get();
    if (!user.exists) return { ok: false, uid, role: null, user: null };
    return { ok: true, role, uid, user: user.data() };
    // return role === 1 || false
  } catch (error) {
    console.log(error);
    return false;
  }
}

module.exports = {
  auth,
  firestore,
  validateToken
};
