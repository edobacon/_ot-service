const functions = require('firebase-functions');

const otInitialDataFn = require('./fns/otInitialDataFn');
const otDataFn = require('./fns/otDataFn');
const changePassword = require('./fns/changePassword');

module.exports = {
  'otinitialdata': functions.https.onRequest(otInitialDataFn),
  'otdata': functions.https.onRequest(otDataFn),
  'changepassword': functions.https.onRequest(changePassword)
};
