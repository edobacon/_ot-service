module.exports = {
  purge: { content: ["./public/**/*.html", "./src/**/*.vue"] },
  darkMode: false, // or 'media' or 'class'
  theme: {
    flexGrow: {
      DEFAULT: 1,
      '0': 0,
      '1': 1,
      '2': 2
    },
    extend: {},
  },
  variants: {
    extend: {
      backgroundColor: ['disabled']
    },
  },
  plugins: [],
};
