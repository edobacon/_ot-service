import moment from 'moment-timezone';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function validateForm(refs: any[] = []): boolean {
  let validForm = true;
  refs.forEach((el) => {
    const valid = el.value?.validationFn();
    if (!valid) validForm = false;
  });
  return validForm;
}

function emailValidator(val: string): boolean {
  const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return pattern.test(val);
}

function dateValidator(
  time: string,
  isTimestamp = false,
  mustBeBeforeToday = false
) {
  if (isTimestamp) return moment(time).isValid();

  const d = time.split('-');
  if (d.join('').split('').length < 8) return false;
  const newDate = moment(`${d[2]}-${d[1]}-${d[0]}`).format();
  if (!moment(newDate).isValid()) return false;
  if (mustBeBeforeToday) {
    const today = moment().format();
    return !moment(newDate).isBefore(today);
  }
  return true;
}

export function rutCleaner(rut: string, soft = true): string {
  let r = rut
    .replace(/\./g, '')
    .replace(/\s/g, '')
    .replace(/^0+/, '')
    .toUpperCase();
  if (!soft) r = r.replace(/-/, '');
  return r;
}

// include dots n dash
export function rutFormatter(v: string): string {
  const val = rutCleaner(v, false);
  const s = val.toString().split('').reverse().join('');
  const dv = s[0];
  const body3 = s.slice(1, 4).split('').reverse().join('');
  const body2 = s.slice(4, 7).split('').reverse().join('');
  const body1 = s.slice(7).split('').reverse().join('');
  return `${body1}.${body2}.${body3}-${dv}`;
}

function rutValidator(rut: string) {
  const r = rutCleaner(rut);
  if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test(r)) return false;
  const tmp = r.split('-');
  let digv = tmp[1];
  const ini = tmp[0];
  if (digv == 'k') digv = 'K';
  return dv(ini).toString() === digv.toString();
}

function dv(T: any) {
  let M = 0,
    S = 1;
  for (; T; T = Math.floor(T / 10)) S = (S + (T % 10) * (9 - (M++ % 6))) % 11;
  return S ? S - 1 : 'K';
}

function numberValidator(val: string): boolean {
  return !isNaN(+val);
}

export function dateToTimestamp(time: string): number {
  const d = time.split('-');
  if (d.join('').split('').length < 8) return 0;
  const newDate = moment(`${d[2]}-${d[1]}-${d[0]}`).format('YYYY-MM-DD');
  return +moment(newDate).format('x');
}

export const inputValidation = (
  value: string,
  validation: string
): { valid: boolean; errorMessage: string } => {
  let valid = true;
  let errorMessage = '';
  switch (validation) {
    case 'required':
      valid = !!(value && value.length > 0);
      !valid && (errorMessage = 'Campo Requerido');
      break;
    case 'dni':
      valid = rutValidator(value);
      !valid && (errorMessage = 'Rut no válido');
      break;
    case 'email':
      valid = emailValidator(value);
      !valid && (errorMessage = 'Email no válido');
      break;
    case 'date':
      valid = dateValidator(value);
      !valid && (errorMessage = 'Fecha no válida');
      break;
    case 'length8':
      valid = value.trim().length > 7;
      !valid && (errorMessage = 'Requiere al menos 8 caracteres');
      break;
    case 'number':
      valid = numberValidator(value);
      !valid && (errorMessage = 'Debe ser número');
      break;
    default:
      valid = true;
      break;
  }
  return { valid, errorMessage };
};

export const checkErrorCode = (code: string): string => {
  switch (code) {
    case 'auth/account-exists-with-different-credential':
      return 'Cuenta existe con credenciales diferentes';
    case 'auth/credential-already-in-use':
      return 'Credenciales ya en uso';
    case 'auth/email-already-in-use':
      return 'Correo ya en uso';
    case 'auth/user-not-found':
      return 'Usuario no encontrado';
    case 'auth/wrong-password':
      return 'Contraseña erronea';
    case 'auth/app-deleted':
      return 'Proyecto no existe';
    case 'auth/app-not-authorized':
      return 'No auth service configurated';
    case 'auth/argument-error':
      return 'Argument error';
    case 'auth/invalid-api-key':
      return 'Apikey inválida';
    case 'auth/invalid-user-token':
      return 'Token de usuario es inválido';
    case 'auth/invalid-tenant-id':
      return 'ID de tenent inválido';
    case 'auth/network-request-failed':
      return 'Error en conexión de red';
    case 'auth/operation-not-allowed':
      return 'Proveedor no habilitado';
    case 'auth/requires-recent-login':
      return 'Requiere de un login más reciente en el proveedor';
    case 'auth/too-many-requests':
      return 'Demasiados intentos, intente nuevamente más tarde';
    case 'auth/unauthorized-domain':
      return 'Dominio no autorizado';
    case 'auth/user-disabled':
      return 'Usuario deshabilitado';
    case 'auth/user-token-expired':
      return 'Token de usuario expirado';
    case 'auth/web-storage-unsupported':
      return 'Navegador no soportado';
    default:
      return "do'h!";
  }
};

export function cleanSource<T>(param: T): T {
  return JSON.parse(JSON.stringify(param));
}
