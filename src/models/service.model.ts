export interface IService {
  description?:  string;
  value?: number | string;
}