export interface IDay {
  date: string;
  dayOfWeek: number;
  month: number;
  day: number;
  year: number;
  timestamp: number;
  label: string;
}

export interface IDayMin {
  timestamp?: number;
  label?: string;
}