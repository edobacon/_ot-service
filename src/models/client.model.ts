export interface IClient {
  name?: string;
  address?: string;
  createdAt?: number;
  updatedAt?: number;
  dni?: string;
  obs?: string;
  email?: string;
  phone?: string;
  createdBy?: string;
  updatedBy?: string;
}