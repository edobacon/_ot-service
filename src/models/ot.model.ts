import { IDayMin } from './calendar.model';
import { IClient } from './client.model';

export enum CLIENT_FORM_STATUS {
  NOT_SEARCH = 'NOT_SEARCH',
  NOT_FOUND = 'NOT_FOUND',
  FOUNDED = 'FOUNDED'
}

export enum APPROVAL_OPTS {
  PENDING = 'PENDING',
  APPROVED = 'APPROVED',
  REJECTED = 'REJECTED',
  OMITTED = 'OMMITED'
}

export function approvalOptsToLabel(status: APPROVAL_OPTS): string {
  switch (status) {
    case APPROVAL_OPTS.PENDING:
      return 'Aprobación Pendiente';
    case APPROVAL_OPTS.APPROVED:
      return 'Aprobado';
    case APPROVAL_OPTS.REJECTED:
      return 'Rechazado';
    case APPROVAL_OPTS.OMITTED:
      return 'Omitido';
    default:
      return '---';
  }
}

export enum CONTACT_OPTS {
  CONTACTED = 'CONTACTED',
  NOT_FOUND = 'NOT_FOUND'
}

export function contactOptsToLabel(status: CONTACT_OPTS): string {
  switch (status) {
    case CONTACT_OPTS.CONTACTED:
      return 'Contactado';
    case CONTACT_OPTS.NOT_FOUND:
      return 'No encontrado';
    default:
      return '---';
  }
}

export enum DELIVERY_OPTS {
  UNESPECIFIED = 'UNESPECIFIED',
  LOCAL_PICKUP = 'LOCAL_PICKUP',
  DELIVERY = 'DELIVERY'
}

export function deliveryOptsToLabel(status: DELIVERY_OPTS): string {
  switch (status) {
    case DELIVERY_OPTS.UNESPECIFIED:
      return 'Sin Especificar';
    case DELIVERY_OPTS.LOCAL_PICKUP:
      return 'Retiro';
    case DELIVERY_OPTS.DELIVERY:
      return 'Envío';
    default:
      return '---';
  }
}

export enum PRODUCT_STATE_OPTS {
  ON_STORE = 'ON_STORE',
  WITHDRAWED = 'WITHDRAWED',
  DELIVERED = 'DELIVERED'
}

export function productStateOptsToLabel(status: PRODUCT_STATE_OPTS): string {
  switch (status) {
    case PRODUCT_STATE_OPTS.DELIVERED:
      return 'Despachado';
    case PRODUCT_STATE_OPTS.WITHDRAWED:
      return 'Retirado';
    case PRODUCT_STATE_OPTS.ON_STORE:
      return 'En Local';
    default:
      return '---';
  }
}

export enum OTSTATUSES {
  ON_REQUEST = 'ON_REQUEST',
  APPROVAL_PENDING = 'APPROVAL_PENDING',
  DIAGNOSIS_APPROVED = 'DIAGNOSIS_APPROVED',
  DIAGNOSIS_REJECTED = 'DIAGNOSIS_REJECTED',
  DIAGNOSIS_OMMITED = 'DIAGNOSIS_OMMITED',
  SERVICES_APPROVAL_PENDING = 'SERVICES_APPROVAL_PENDING',
  SERVICES_APPROVED = 'SERVICES_APPROVED',
  SERVICES_REJECTED = 'SERVICES_REJECTED',
  DIAGNOSIS_WORKING = 'DIAGNOSIS_WORKING',
  DIAGNOSIS_FINISHED = 'DIAGNOSIS_FINISHED',
  WORKSHOP_WORKING = 'WORKSHOP_WORKING',
  WORKSHOP_FINISHED = 'WORKSHOP_FINISHED',
  CLIENT_NOT_FOUND = 'CLIENT_NOT_FOUND',
  TO_LOCAL_PICKUP = 'TO_LOCAL_PICKUP',
  TO_DELIVERY = 'TO_DELIVERY',
  WITHDRAWED = 'WITHDRAWED',
  ON_LOCAL = 'ON_LOCAL',
  DELIVERED = 'DELIVERED'
}

export function otStatusToLabel(status: OTSTATUSES): string {
  switch (status) {
    case OTSTATUSES.ON_REQUEST:
      return 'En ingreso';
    case OTSTATUSES.APPROVAL_PENDING:
      return 'Aprobación Pendiente';
    case OTSTATUSES.DIAGNOSIS_APPROVED:
      return 'Diagnóstico Aprobado';
    case OTSTATUSES.DIAGNOSIS_REJECTED:
      return 'Diagnóstico Rechazada';
    case OTSTATUSES.DIAGNOSIS_WORKING:
      return 'En Diagnóstico';
    case OTSTATUSES.DIAGNOSIS_FINISHED:
      return 'Diagnóstico Terminado';
    case OTSTATUSES.SERVICES_APPROVED:
      return 'Servicio Aprobado';
    case OTSTATUSES.SERVICES_REJECTED:
      return 'Servicio Rechazado';
    case OTSTATUSES.SERVICES_APPROVAL_PENDING:
    return 'Servicio Pendiente Aprobación';
    case OTSTATUSES.WORKSHOP_WORKING:
      return 'En Servicio';
    case OTSTATUSES.WORKSHOP_FINISHED:
      return 'Servicio Completado';
    case OTSTATUSES.CLIENT_NOT_FOUND:
      return 'Cliente no Contactado';
    case OTSTATUSES.TO_LOCAL_PICKUP:
      return 'Retiro Solicitado';
    case OTSTATUSES.TO_DELIVERY:
      return 'Despacho Solicitado';
    case OTSTATUSES.WITHDRAWED:
      return 'Retirado';
    case OTSTATUSES.ON_LOCAL:
      return 'En Bodega';
    case OTSTATUSES.DELIVERED:
      return 'Despachado';
    default:
      return 'En ingreso';
  }
}

export interface IOTState {
  failureDeclaration?: string;
  productDetailsOnArrive?: string;
  completed?: boolean;
}

export interface IOTService {
  description?: string;
  value?: number;
}

export interface IOTServices {
  services: IOTService[];
  diagnosisDate?: IDayMin;
  deliveryDate?: IDayMin;
  obs: string;
  completed?: boolean;
  lastUpdateBy?: string;
}

export interface IOTApproval {
  diagnosisApproval?: APPROVAL_OPTS;
  servicesApproval?: APPROVAL_OPTS;
  obs?: string;
  completed?: boolean;
  lastUpdateBy?: string;
}

export interface IOTWorkshop {
  startedAt?: IDayMin;
  finishedAt?: IDayMin;
  obs?: string;
  completed?: boolean;
  lastUpdateBy?: string;
}

export interface IOTDelivery {
  contacted?: CONTACT_OPTS;
  contactedObs?: string;
  deliveryType?: DELIVERY_OPTS;
  deliveryTypeObs?: string;
  completed?: boolean;
  lastUpdateBy?: string;
}

export interface IOTPayment {
  paymentCode?: string;
  withdrawStatus?: PRODUCT_STATE_OPTS;
  obs: string;
  completed?: boolean;
  lastUpdateBy?: string;
}

export interface IOT {
  statusParsed?: { status: OTSTATUSES; label: string };
  status?: OTSTATUSES;
  uid?: string | null;
  id?: string;
  clientDni?: string;
  clientUID?: string | null;
  client?: IClient | null;
  state?: IOTState | null;
  services?: IOTServices | null;
  approval?: IOTApproval | null;
  diagnosis?: IOTWorkshop | null;
  workshop?: IOTWorkshop | null;
  delivery?: IOTDelivery | null;
  payment?: IOTPayment | null;
  createdAt?: number;
  updatedAt?: number;
  deliveryAt?: number;
  diagnosisAt?: number;
  diagnosisAtParsed?: IDayMin;
  deliveryAtParsed?: IDayMin;
  createdBy?: string;
  lastUpdateBy?: string;
}
