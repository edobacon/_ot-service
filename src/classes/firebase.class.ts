import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import { toastController } from '@/store/toast.store';
import { Ref, ref } from '@vue/reactivity';
import { checkErrorCode } from '@/utils';

export interface IDbResponse<T> {
  ok: boolean;
  payload?: T;
}

export interface IUser {
  firstName?: string;
  lastName?: string;
  email?: string;
  uid?: string;
  role?: string;
  created_at?: number;
}

export interface IGenericDoc<T> {
  uid: string;
  data?: T;
}

export interface IGenericCollection<T> {
  length: number;
  docs: IGenericDoc<T>[];
  next: boolean;
  last?: firebase.firestore.DocumentSnapshot<T>;
}

interface IAuthStatus {
  loggedIn: boolean;
  uid: string;
  user?: IUser;
}

export interface IFunctionResponse<T> {
  ok: boolean;
  status: number;
  body: T;
}

export interface ICollectionQuery<T> {
  limit?: number;
  order?: { key: string; type: 'asc' | 'desc' };
  query?: {
    key: string;
    op:
      | '<'
      | '<='
      | '=='
      | '!='
      | '>='
      | '>'
      | 'array-contains'
      | 'in'
      | 'array-contains-any'
      | 'not-in';
    value: any;
  }[];
  startAfter?: firebase.firestore.DocumentSnapshot<T>;
}

const converter = <T>() => ({
  toFirestore: (data: T) => data,
  fromFirestore: (snap: firebase.firestore.QueryDocumentSnapshot) =>
    snap.data() as T
});

export class Firebase {
  private static instance: Firebase;
  private au: firebase.auth.Auth;
  private db: firebase.firestore.Firestore;
  private uid = ref('');
  private currentUser: Ref<IUser | null> = ref(null);
  private isLoggedIn = ref(false);
  private loading = ref(false);

  get authStatus(): IAuthStatus {
    const data: IAuthStatus = {
      loggedIn: this.isLoggedIn.value,
      uid: this.uid.value
    };
    this.currentUser.value && (data.user = this.currentUser.value);
    return data;
  }
  get loadingState(): boolean {
    return this.loading.value;
  }
  get currentUID(): string | undefined {
    return this.au.currentUser?.uid;
  }
  get role(): string {
    return this.currentUser.value?.role + '';
  }

  set uidSetter(uid: string) {
    this.uid.value = uid;
  }

  private constructor() {
    firebase.initializeApp({
      apiKey: process.env.FB_APIKEY,
      authDomain: process.env.FB_AUTH_DOMAIN,
      projectId: process.env.FB.PROJECT_ID,
      storageBucket: process.env.STORAGE_BUCKET,
      messagingSenderId: process.env.FB_MESSAGING_SENDER_ID,
      appId: process.env.FB_APP_ID,
      measurementId: process.env.FB_MEASUREMENT_ID
    });
    this.au = firebase.auth();
    this.db = firebase.firestore();
  }
  public static getInstance(): Firebase {
    if (!Firebase.instance) {
      Firebase.instance = new Firebase();
    }

    return Firebase.instance;
  }
  async signUserByEmail(email: string, password: string): Promise<boolean> {
    try {
      this.loading.value = true;
      const sign = await this.au.signInWithEmailAndPassword(email, password);
      if (sign) {
        await this.fetchMe(sign.user?.uid);
        toastController({ text: 'Sesión Iniciada', status: 'success' });
        return true;
      } else {
        toastController({ text: 'Error iniciando sesión', status: 'danger' });
        this.isLoggedIn.value = false;
      }
      return this.isLoggedIn.value;
    } catch (error) {
      console.log(error);
      toastController({ text: checkErrorCode(error.code), status: 'danger' });
      return false;
    } finally {
      this.loading.value = false;
    }
  }
  async passwordRecovery(email: string): Promise<void> {
    try {
      this.loading.value = true;
      await this.au.sendPasswordResetEmail(email);
      toastController({ text: 'Email enviado', status: 'success' });
    } catch (error) {
      console.log(error);
      toastController({ text: 'Error generando email de recuperación', status: 'danger' });
    } finally {
      this.loading.value = false;
    }
  }
  async fetchMe(uid?: string): Promise<IDbResponse<IUser>> {
    if (!uid && !this.uid.value) return { ok: false };
    if (uid) this.uid.value = uid;
    const doc = await this.db
      .doc(`users/${this.uid.value}`)
      .withConverter(converter<IUser>())
      .get();
    if (doc.exists) {
      await this.au.setPersistence(firebase.auth.Auth.Persistence.SESSION);
      const me = {
        uid: doc.id,
        ...doc.data()
      };
      this.currentUser.value = me;
      this.uid.value = me.uid;
      this.isLoggedIn.value = true;
      const response: IDbResponse<IUser> = {
        ok: true,
        payload: me
      };
      return response;
    }
    await this.signOut();
    return { ok: false };
  }
  async signOut(): Promise<void> {
    await this.au.signOut();
    this.uid.value = '';
    this.isLoggedIn.value = false;
    this.currentUser.value = null;
  }
  private async getUserToken(): Promise<string> {
    if (!this.isLoggedIn.value || !this.au.currentUser) return '';
    return await this.au.currentUser.getIdToken(true);
  }
  async callFunction<T>(
    url: string,
    data?: { [k: string]: unknown }
  ): Promise<IFunctionResponse<T> | null> {
    try {
      this.loading.value = true;
      const token = await this.getUserToken();
      const base =
        process.env.NODE_ENV === 'development'
        ? process.env.FB_FUNCTIONS_EMULATOR_URI
        : process.env.FB_FUNCTIONS_PROJECT_URI;
      const response = await fetch(base + url, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify({ token, ...data })
      });
      const body: T = await response.json();
      return {
        ok: response.ok,
        status: response.status,
        body
      };
    } catch (error) {
      console.log(error);
      return null;
    } finally {
      this.loading.value = false;
    }
  }
  async getDoc<T>(path: string, doc: string): Promise<IGenericDoc<T> | null> {
    try {
      this.loading.value = true;
      const request = await this.db
        .doc(`${path}/${doc}`)
        .withConverter(converter<T>())
        .get();
      const data: IGenericDoc<T> = {
        uid: request.id,
        data: request.data()
      };
      return data;
    } catch (error) {
      console.log(error);
      return null;
    } finally {
      this.loading.value = false;
    }
  }
  async addDoc<T>(path: string, doc: T): Promise<IGenericDoc<T> | null> {
    try {
      this.loading.value = true;
      const request = await this.db
        .collection(path)
        .withConverter(converter<T>())
        .add(doc);
      const data: IGenericDoc<T> = {
        uid: request.id,
        data: doc
      };
      return data;
    } catch (error) {
      console.log(error);
      return null;
    } finally {
      this.loading.value = false;
    }
  }
  async updateDoc<T>(path: string, id: string, doc: T): Promise<boolean> {
    try {
      this.loading.value = true;
      await this.db
        .doc(`${path}/${id}`)
        .withConverter(converter<T>())
        .set(doc, { merge: true });
      return true;
    } catch (error) {
      console.log(error);
      return false;
    } finally {
      this.loading.value = false;
    }
  }
  async deleteDoc(path: string, doc: string): Promise<boolean> {
    try {
      this.loading.value = true;
      await this.db.collection(path).doc(doc).delete();
      return true;
    } catch (error) {
      console.log(error);
      return false;
    } finally {
      this.loading.value = false;
    }
  }
  async getCollection<T>(
    path: string,
    params: ICollectionQuery<T>
  ): Promise<IGenericCollection<T> | null> {
    try {
      this.loading.value = true;
      let ref:
        | firebase.firestore.CollectionReference<T>
        | firebase.firestore.Query<T> = this.db
        .collection(path)
        .withConverter(converter<T>());
      const { limit, order, query, startAfter } = params;
      if (limit) ref = ref.limit(limit);
      if (order) ref = ref.orderBy(order.key, order.type);
      if (query)
        query.forEach((el) => (ref = ref.where(el.key, el.op, el.value)));

      if (startAfter) ref = ref.startAfter(startAfter);
      if (startAfter && !limit) ref = ref.limit(10);
      const request = await ref.get();
      const docs = request.docs.map((el) => {
        return {
          uid: el.id,
          data: el.data()
        };
      });
      return {
        length: request.size,
        next: request.docs.length > 0,
        docs,
        last: request.docs[request.size - 1]
      };
    } catch (error) {
      console.log(error);
      return null;
    } finally {
      this.loading.value = false;
    }
  }
  subscribeToColletion<T>(
    path: string,
    params: ICollectionQuery<T>,
    cb: (param: IGenericDoc<T>[]) => void
  ): () => void {
    try {
      this.loading.value = true;
      let ref: | firebase.firestore.CollectionReference<T>
      | firebase.firestore.Query<T> = this.db.collection(path).withConverter(converter<T>());
      const { limit, order, query, startAfter } = params;
      if (limit) ref = ref.limit(limit);
      if (order) ref = ref.orderBy(order.key, order.type);
      if (query) query.forEach((el) => ref = ref.where(el.key, el.op, el.value));
      if (startAfter) ref = ref.startAfter(startAfter);
      if (startAfter && !limit) ref = ref.limit(10);
      const unsubscribe = ref.onSnapshot((snap) => {
        const data = snap.docs.map((el) => {
          const d: IGenericDoc<T> = { uid: el.id, data: el.data() };
          return d;
        });
        cb(data);
      });
      return unsubscribe;
    } catch (error) {
      console.log(error);
      return () => null;
    } finally {
      this.loading.value = false;
    }
  }
}
