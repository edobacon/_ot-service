import { IDayMin } from '@/models/calendar.model';
import { IClient } from '@/models/client.model';
import {
  APPROVAL_OPTS,
  CONTACT_OPTS,
  DELIVERY_OPTS,
  IOT,
  IOTApproval,
  IOTDelivery,
  IOTPayment,
  IOTServices,
  IOTState,
  IOTWorkshop,
  OTSTATUSES,
  otStatusToLabel,
  PRODUCT_STATE_OPTS
} from '@/models/ot.model';
import { Ref, ref } from '@vue/reactivity';
import moment from 'moment-timezone';
import { Firebase, IGenericDoc } from './firebase.class';

const fb = Firebase.getInstance();

export class OT {
  private _status: Ref<OTSTATUSES> = ref(OTSTATUSES.ON_REQUEST);
  private _completed = false;
  private _id = '';
  private _uid: string | null = null;
  private _clientUID: string | null = null;
  private _clientDni = '';
  private _client: IClient | null = null;
  private _state: IOTState | null = null;
  private _services: IOTServices | null = null;
  private _approval: IOTApproval | null = null;
  private _diagnosis: IOTWorkshop | null = null;
  private _workshop: IOTWorkshop | null = null;
  private _delivery: IOTDelivery | null = null;
  private _payment: IOTPayment | null = null;
  private _createdAt: IDayMin | null = null;
  private _updatedAt: IDayMin | null = null;
  private _diagnosisAt: IDayMin | null = null;
  private _deliveryAt: IDayMin | null = null;
  constructor(clientUID?: string | null) {
    clientUID && (this._clientUID = null);
  }
  async setClient(uid: string, client?: IClient): Promise<void> {
    if (client && uid) {
      this._clientUID = uid;
      this._client = client;
    } else if (!uid && client?.dni) {
      await this.setClientByDni(client.dni);
    } else if (uid && !client) {
      await this.setClientByUid(uid);
    }
  }
  get status(): { status: OTSTATUSES; label: string } {
    const status = this._status.value
      ? this._status.value
      : OTSTATUSES[this._status.toString() as keyof typeof OTSTATUSES];
    return { status, label: otStatusToLabel(status) };
  }
  get id(): string {
    return this._id;
  }
  set id(id: string) {
    this._id = id;
  }

  get uid(): string {
    return this._uid ? this._uid : '';
  }
  set uid(uid: string) {
    this._uid = uid;
  }

  set state(state: IOTState) {
    if (!state) return;
    this._state = state;
    this._state.completed = false;
    this._state.productDetailsOnArrive && (this._state.completed = true);
    this.checkCompleteStatus();
  }
  set services(services: IOTServices) {
    if (!services) return;
    this._services = services;
    services.diagnosisDate?.timestamp &&
      !services.diagnosisDate?.label &&
      (this._services.diagnosisDate = this.timeParser(
        services.diagnosisDate.timestamp
      ));
    services.deliveryDate?.timestamp &&
      !services.deliveryDate?.label &&
      (this._services.deliveryDate = this.timeParser(
        services.deliveryDate.timestamp
      ));
    this._services.completed = true;
    if (
      this._status.value === OTSTATUSES.APPROVAL_PENDING ||
      this._status.value === OTSTATUSES.ON_REQUEST
    ) {
      this._clientUID &&
        this._state?.completed &&
        this._services.completed &&
        (this._status.value = OTSTATUSES.APPROVAL_PENDING);
    }
    //
    this.checkCompleteStatus();
  }
  set approval(ap: IOTApproval) {
    if (!ap) return;
    this._approval = ap;
    if (
      this._status.value === OTSTATUSES.DIAGNOSIS_WORKING ||
      this._status.value === OTSTATUSES.WORKSHOP_FINISHED
    )
      return;
    if (!this._diagnosis?.completed && !this._workshop?.completed) {
      switch (ap.diagnosisApproval) {
        case 'PENDING':
          this._status.value = OTSTATUSES.APPROVAL_PENDING;
          this._approval.completed = false;
          break;
        case 'APPROVED':
          this._status.value = OTSTATUSES.DIAGNOSIS_APPROVED;
          break;
        case 'REJECTED':
          this._status.value = OTSTATUSES.DIAGNOSIS_REJECTED;
          this._approval.completed = true;
          break;
        default:
          this._status.value = OTSTATUSES.APPROVAL_PENDING;
          this._approval.completed = false;
          break;
      }
    }
    if (
      this._status.value !== OTSTATUSES.WORKSHOP_WORKING &&
      ap.diagnosisApproval &&
      !this._workshop?.completed &&
      ((this._diagnosis?.completed &&
        ap.diagnosisApproval === APPROVAL_OPTS.APPROVED) ||
        ap.diagnosisApproval === APPROVAL_OPTS.OMITTED)
    ) {
      switch (ap.servicesApproval) {
        case 'PENDING':
          this._status.value = OTSTATUSES.SERVICES_APPROVAL_PENDING;
          this._approval.completed = false;
          break;
        case 'APPROVED':
          this._status.value = OTSTATUSES.SERVICES_APPROVED;
          this._approval.completed = true;
          break;
        case 'REJECTED':
          this._status.value = OTSTATUSES.SERVICES_REJECTED;
          this._approval.completed = true;
          break;
        default:
          this._status.value = OTSTATUSES.SERVICES_APPROVAL_PENDING;
          this._approval.completed = false;
          break;
      }
    }
    this.checkCompleteStatus();
  }
  set diagnosis(dg: IOTWorkshop) {
    if (!dg) return;
    this._diagnosis = dg;
    // if (this._diagnosis.completed) return;
    this._diagnosis.completed = false;
    dg.startedAt?.timestamp &&
      !dg.finishedAt?.timestamp &&
      (this._status.value = OTSTATUSES.DIAGNOSIS_WORKING);
    dg.startedAt?.timestamp &&
      !dg.startedAt?.label &&
      (this._diagnosis.startedAt = this.timeParser(
        dg.startedAt.timestamp,
        true
      ));
    dg.finishedAt?.timestamp &&
      !dg.finishedAt?.label &&
      (this._diagnosis.finishedAt = this.timeParser(
        dg.finishedAt.timestamp,
        true
      ));
    if (dg.startedAt?.timestamp && dg.finishedAt?.timestamp) {
      this._status.value = OTSTATUSES.DIAGNOSIS_FINISHED;
      this._diagnosis.completed = true;
    }
    this.checkCompleteStatus();
  }
  set workshop(ws: IOTWorkshop) {
    if (!ws) return;
    this._workshop = ws;
    // if (this._workshop.completed) return;
    this._workshop.completed = false;
    ws.startedAt?.timestamp &&
      !ws.finishedAt?.timestamp &&
      (this._status.value = OTSTATUSES.WORKSHOP_WORKING);
    ws.startedAt?.timestamp &&
      !ws.startedAt?.label &&
      (this._workshop.startedAt = this.timeParser(ws.startedAt.timestamp));
    ws.finishedAt?.timestamp &&
      !ws.finishedAt?.label &&
      (this._workshop.finishedAt = this.timeParser(ws.finishedAt.timestamp));
    if (ws.startedAt?.timestamp && ws.finishedAt?.timestamp) {
      this._workshop.completed = true;
      this._status.value = OTSTATUSES.WORKSHOP_FINISHED;
    }
    this.checkCompleteStatus();
  }
  set delivery(de: IOTDelivery) {
    if (!de) return;
    this._delivery = de;
    if (de.contacted === CONTACT_OPTS.CONTACTED && de.deliveryType)
      this._delivery.completed = true;
    else this._delivery.completed = false;
    if (de.contacted === CONTACT_OPTS.NOT_FOUND)
      this._status.value = OTSTATUSES.CLIENT_NOT_FOUND;
    else if (de.contacted == CONTACT_OPTS.CONTACTED) {
      if (de.deliveryType === DELIVERY_OPTS.DELIVERY)
        this._status.value = OTSTATUSES.TO_DELIVERY;
      else if (de.deliveryType === DELIVERY_OPTS.LOCAL_PICKUP)
        this._status.value = OTSTATUSES.TO_LOCAL_PICKUP;
    }
    this.checkCompleteStatus();
  }
  set payment(payment: IOTPayment) {
    if (!payment) return;
    this._payment = payment;
    if (
      !payment.withdrawStatus ||
      payment.withdrawStatus === PRODUCT_STATE_OPTS.ON_STORE
    )
      this._payment.completed = false;
    else this._payment.completed = true;
    switch (payment.withdrawStatus) {
      case PRODUCT_STATE_OPTS.ON_STORE:
        this._status.value = OTSTATUSES.ON_LOCAL;
        break;
      case PRODUCT_STATE_OPTS.WITHDRAWED:
        this._status.value = OTSTATUSES.WITHDRAWED;
        break;
      case PRODUCT_STATE_OPTS.DELIVERED:
        this._status.value = OTSTATUSES.DELIVERED;
        break;
      default:
        break;
    }
    this.checkCompleteStatus();
  }

  async setClientByDni(dni: string): Promise<boolean> {
    const clients = await fb.getCollection<IClient>('clients', {
      query: [{ key: 'dni', op: '==', value: dni }],
      limit: 1
    });
    this._clientDni = dni;
    if (clients && clients.docs.length > 0) {
      this._clientUID = clients.docs[0].uid;
      clients.docs[0].data && (this._client = clients.docs[0].data);
      return true;
    }
    return false;
  }
  async setClientByUid(uid: string): Promise<boolean> {
    const client = await fb.getDoc<IClient>('clients', uid);
    if (client && client.data) {
      this._clientUID = uid;
      this._client = client.data;
      client.data.dni && (this._clientDni = client.data.dni);
      return true;
    }
    return false;
  }
  async saveOT(): Promise<void> {
    console.log('SAVING !');
  }
  get data(): {
    ot: IOT;
    completed: boolean;
    status: { status: OTSTATUSES; label: string };
    createdAt: IDayMin | null;
    updatedAt: IDayMin | null;
  } {
    const ot: IOT = {
      uid: this._uid,
      id: this._id,
      clientUID: this._clientUID,
      clientDni: this._clientDni,
      client: this._client,
      state: this._state,
      services: this._services,
      diagnosis: this._diagnosis,
      workshop: this._workshop,
      approval: this._approval,
      delivery: this._delivery,
      payment: this._payment
    };
    this._diagnosisAt && (ot.diagnosisAtParsed = this._diagnosisAt);
    this._deliveryAt && (ot.deliveryAtParsed = this._deliveryAt);
    return {
      completed: this._completed,
      ot,
      status: this.status,
      createdAt: this._createdAt,
      updatedAt: this._updatedAt
    };
  }
  static fromFirebaseDoc(doc: IGenericDoc<IOT>): OT {
    const ot = new OT();
    ot.uid = doc.uid;
    doc.data?.id && (ot.id = doc.data?.id);
    doc.data?.clientUID && (ot._clientUID = doc.data?.clientUID);
    doc.data?.clientDni && (ot._clientDni = doc.data?.clientDni);
    doc.data?.state && (ot.state = doc.data?.state);
    doc.data?.services && (ot.services = doc.data?.services);
    doc.data?.diagnosis && (ot.diagnosis = doc.data?.diagnosis);
    doc.data?.workshop && (ot.workshop = doc.data?.workshop);
    doc.data?.approval && (ot.approval = doc.data?.approval);
    doc.data?.delivery && (ot.delivery = doc.data?.delivery);
    doc.data?.payment && (ot.payment = doc.data?.payment);
    doc.data?.createdAt &&
      (ot._createdAt = ot.timeParser(doc.data?.createdAt, true));
    doc.data?.updatedAt &&
      (ot._updatedAt = ot.timeParser(doc.data?.updatedAt, true));
    doc.data?.diagnosisAt &&
      (ot._diagnosisAt = ot.timeParser(doc.data?.diagnosisAt, false));
    doc.data?.deliveryAt &&
      (ot._deliveryAt = ot.timeParser(doc.data?.deliveryAt, false));
    doc.data?.status && (ot._status.value = doc.data.status);
    return ot;
  }
  private checkCompleteStatus() {
    this._completed =
      this._status.value === OTSTATUSES.DELIVERED ||
      this._status.value === OTSTATUSES.WITHDRAWED;
  }
  private timeParser(timestamp: number, includeHour = false): IDayMin {
    return {
      timestamp,
      label: moment.utc(timestamp).format(
        includeHour ? 'DD MMM YYYY HH:mm' : 'DD MMM YYYY'
      )
    };
  }
}
