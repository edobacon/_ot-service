import { createApp, App as Application } from 'vue';
import App from './App.vue';

import router from './router';

import Harlem from '@harlem/core';
import createDevtoolsPlugin from '@harlem/plugin-devtools';

import './css/tailwind.css';
import './css/styles.css';

import Divider from '@/components/UI/Divider.vue'; 

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import {
  faAt,
  faExclamationCircle,
  faCheckCircle,
  faTimesCircle,
  faInfoCircle,
  faChevronLeft,
  faChevronRight,
  faBars,
  faTimes,
  faUsers,
  faFileInvoice,
  faUsersCog,
  faFilter,
  faSearch,
  faCalendar,
  faFilePdf,
  faFileSignature,
  faCalendarWeek,
  faCheck,
  faPen
} from '@fortawesome/free-solid-svg-icons';
import {
  faEye,
  faEyeSlash,
  faFileAlt,
  faUserCircle
} from '@fortawesome/free-regular-svg-icons';
library.add(
  faAt,
  faExclamationCircle,
  faCheckCircle,
  faTimesCircle,
  faInfoCircle,
  faChevronLeft,
  faChevronRight,
  faBars,
  faTimes,
  faFileAlt,
  faUserCircle,
  faUsers,
  faFileInvoice,
  faUsersCog,
  faFilter,
  faEye,
  faEyeSlash,
  faSearch,
  faCalendar,
  faFilePdf,
  faFileSignature,
  faCalendarWeek,
  faCheck,
  faPen
);

// import './plugins/firebase';
import './plugins/moment';

import { Firebase } from './classes/firebase.class';
const fb = Firebase.getInstance();
import firebase from 'firebase/app';
import 'firebase/auth';

let app: Application;

const plugins: any[] = [];
if (process.env.NODE_ENV === 'development') {
  plugins.push(
    createDevtoolsPlugin({
      label: 'State'
    })
  );
}


firebase.auth().onAuthStateChanged(user => {
  user?.uid && (fb.uidSetter = user.uid);
  if (!app) {
    app = createApp(App);
    app.component('fa-icon', FontAwesomeIcon);
    app.component('divider', Divider);
    app.use(router).use(Harlem, { plugins });
    app.mount('#app');
  }
  
});

