import { createStore } from '@harlem/core';

interface IModal {
  show: boolean;
  component?: string;
}

interface IModalState {
  show: boolean;
  back: boolean;
  component: string;
}

const STATE: IModalState = {
  show: false,
  back: false,
  component: 'loading'
};

const { getter, mutation } = createStore('modal', STATE);

export const modalController = mutation<IModal>(
  'modalController',
  (state, payload) => {
    const { show, component } = payload;
    state.show = show;
    component? state.component = component : state.component = 'loading';
    const html = document.querySelector('html');
    const body = document.querySelector('body');
    if (show) {
      html?.classList.add('overflow-hidden');
      body?.classList.add('overflow-hidden');
    } else {
      html?.classList.remove('overflow-hidden');
      body?.classList.remove('overflow-hidden');
    }
    setTimeout(() => (state.back = show), 550);
  }
);

export const modalState = getter('modalState', (state) => state);
