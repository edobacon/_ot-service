import { createStore } from '@harlem/core';

interface IUser {
  firstName: string;
  lastName: string; 
  email: string;
  uid: string;
  role: string;
}

interface IUsersState {
  user: IUser | null;
}

const STATE: IUsersState = {
  user: null
};

const { getter, mutation } = createStore('user', STATE);

export const setUser = mutation<IUser>('setUser', (state, payload) => {
  state.user = payload;
});

export const userState = getter('userState', state => state.user);