import { createStore } from '@harlem/core';

interface ISetupState {
  setupCompleted: boolean;
}

const STATE: ISetupState = {
  setupCompleted: true
};

const { getter, mutation } = createStore('setup', STATE);

export const setSetupStatus = mutation<boolean>('setSetupStatus', (state, payload) => {
  state.setupCompleted = payload;
});

export const setupCompleted = getter('setupState', state => state.setupCompleted);