import { createStore } from '@harlem/core';

interface IToastState {
  show: boolean;
  text: string;
  duration: number;
  color: string;
  clickable: string;
  callback?:() => void;
  callbackDone: boolean;
  icon: string[];
}

const STATE: IToastState = {
  show: false,
  text: 'label',
  duration: 3000,
  color: 'bg-gray-600',
  clickable: 'select-none',
  callbackDone: false,
  icon: ['fa','check'],
  callback: undefined
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const { getter, mutation } = createStore('toast', STATE);

// on(EVENTS.mutation.before, event => console.log(event))
// on(EVENTS.mutation.after, event => console.log(event?.data.mutation))
// on(EVENTS.mutation.error, event => console.log(event))

// MUTATIONS

interface IToastOptions {
  text: string;
  status: 'success' | 'danger' | 'info' | 'alert';
  callback?(): void;
  duration?: number;
}

export const toastController = mutation<IToastOptions>(
  'showToast',
  (state, payload) => {
    if (state.show === true) return;
    payload.duration && (state.duration = payload.duration);
    state.clickable = payload.callback ? 'cursor-pointer' : 'select-none';
    state.text = payload.text;
    if (payload.callback)
      state.callback = function() {
        if (payload.callback && !state.callbackDone && state.show) {
          payload.callback();
        }
        state.callbackDone = true;
        state.show = false;
      };
    switch (payload.status) {
      case 'success':
        state.color = 'bg-green-600';
        state.icon = ['fas', 'check-circle'];
        break;
      case 'danger':
        state.color = 'bg-red-800';
        state.icon = ['fas', 'times-circle'];
        break;
      case 'alert':
        state.color = 'bg-yellow-600';
        state.icon = ['fas', 'exclamation-circle'];
        break;
      case 'info':
        state.color = 'bg-blue-600';
        state.icon = ['fas', 'info-circle'];
        break;
      default:
        state.color = 'bg-gray-600';
        state.icon = ['fas', 'info-circle'];
        break;
    }
    state.show = true;
    setTimeout(() => {
      state.show = false;
      state.callbackDone = false;
    }, state.duration);
  }
);

// GETTERS

export const toastState = getter('toastState', state => {
  return {
    show: state.show,
    color: state.color,
    clickable: state.clickable,
    text: state.text,
    icon: state.icon,
    callback: state.callback
  };
});
