import { Firebase } from '@/classes/firebase.class';
import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'login',
    component: () =>
      import(/* webpackChunkName: "login" */ '../views/Login.vue'),
    meta: {
      private: false,
      fullName: 'Login'
    }
  },
  {
    path: '/app',
    component: () =>
      import(
        /* webpackChunkName: "sidebar-layout" */ '../layouts/SidebarLayout.vue'
      ),
    children: [
      {
        path: '/ots',
        name: 'app.ots',
        component: () =>
          import(/* webpackChunkName: "app-ots" */ '../views/Ots.vue'),
        meta: {
          private: true,
          index: 0,
          fullName: 'Órdenes de Trabajo',
        }
      },
      {
        path: '/clients',
        name: 'app.clients',
        component: () =>
          import(/* webpackChunkName: "app-clients" */ '../views/Clients.vue'),
        meta: {
          private: true,
          index: 1,
          fullName: 'Clientes'
        }
      },
      {
        path: '/account',
        name: 'app.account',
        component: () =>
          import(/* webpackChunkName: "app-account" */ '../views/Account.vue'),
        meta: {
          private: true,
          index: 2,
          fullName: 'Mi Cuenta'
        }
      }
    ]
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

const fb = Firebase.getInstance();
router.beforeEach(async (to, _from, next) => {
  if (to.meta.private) {
    const status = fb.authStatus;
    if (status.loggedIn) return next(); 
    else if (status.uid) {
      const user = await fb.fetchMe(status.uid);
      if (user) return next();
    }
    return next('/');
  }
  return next();
});


export default router;
